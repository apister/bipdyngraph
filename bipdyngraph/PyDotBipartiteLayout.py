import pydot

import bipdyngraph.BipDynGraph as BipDynGraph
from bipdyngraph.PyDotLayout import PydotLayout
from bipdyngraph.globals import ENTITYTYPE_KEY, EDGETYPE_KEY


class PydotBipartiteLayout(PydotLayout):
    def __init__(self, bip_dyn_graph: BipDynGraph.BipartiteDynGraph):
        super().__init__(bip_dyn_graph)

        self.PERSON_JUMPS = True
        self.ALIGN_PERSONS = False

        self.SAME_PERSON_EDGE_WEIGHT = 10

    def build_graph(self):
        self.time_to_subgraph = {}
        self.time_to_ranks = {}

        self.add_time_nodes()
        self.add_person_lines()
        self.add_documents()

    def add_time_nodes(self):
        for i in range(len(self.graph.times)):
            if i < len(self.graph.times) - 1:
                edge_between_ts = pydot.Edge(f"{self.graph.times[i]}_after", f"{self.graph.times[i + 1]}_before")
                self.pydot_graph.add_edge(edge_between_ts)

            edge_ts = pydot.Edge(f"{self.graph.times[i]}_before", f"{self.graph.times[i]}_after")
            self.pydot_graph.add_edge(edge_ts)

            node_ts_before = pydot.Node(f'{self.graph.times[i]}_before')
            node_ts_after = pydot.Node(f'{self.graph.times[i]}_after')

            subgraph_ts_before = pydot.Subgraph(f"{i}_before", rank="same")
            subgraph_ts_after = pydot.Subgraph(f"{i}_after", rank="same")

            subgraph_ts_before.add_node(node_ts_before)
            subgraph_ts_after.add_node(node_ts_after)

            self.time_to_subgraph[self.graph.times[i]] = (subgraph_ts_before, subgraph_ts_after)
            # self.time_to_anchors[self.graph.times[i]] = (node_ts_before, node_ts_after)
            self.time_to_ranks[self.graph.times[i]] = (i * 2, i * 2 + 1)

    #TODO: add one layer before first time ?
    def add_person_lines(self):
        for person in self.graph.persons():
            times = self.graph.person_times(person)
            for i, time in enumerate(times[:-1]):
                subgraph_time = self.time_to_subgraph[time][1]
                rank = self.time_to_ranks[time][1]

                node_id = self.generate_person_unique_node_id(person, time)
                if self.ALIGN_PERSONS:
                    person_node = pydot.Node(node_id, group=person, rank=rank)
                else:
                    person_node = pydot.Node(node_id, rank=rank)
                self.add_node(person_node, subgraph_time)

                if not (self.PERSON_JUMPS):
                    if times[i + 1] - times[i] > 1:
                        continue

                node_t2_id = self.generate_person_unique_node_id(person, times[i + 1])
                edge = pydot.Edge(node_id, node_t2_id, style="dotted",
                                  weight=self.SAME_PERSON_EDGE_WEIGHT, **{EDGETYPE_KEY: "person_time"})
                self.pydot_graph.add_edge(edge)

    # def create_unique_persons(self):
    #     subgraph_persons = pydot.Subgraph(f"persons", rank="same")
    #     anchor_node = pydot.Node("anchor")
    #     subgraph_persons.add_node(anchor_node)
    #
    #     anchor_edge = pydot.Edge("anchor", list(self.time_to_anchors.values())[0][0])
    #     self.pydot_graph.add_edge(anchor_edge)
    #
    #     for person in self.graph.persons():
    #         node = pydot.Node(self.unique_person_node_id(person))
    #         subgraph_persons.add_node(node)
    #     self.pydot_graph.add_subgraph(subgraph_persons)

    def add_documents(self):
        for i, time in enumerate(self.graph.times):
            # if i == 6:
            #     break

            rank_before, rank_after = self.time_to_subgraph[time]

            documents = self.graph.documents_at_time(time)
            for document in documents:
                document_node_id = self.generate_document_node_id(document, time)
                rank = self.time_to_ranks[time][0]

                # If every node is an ellipse, it's easier to parse after
                document_node = pydot.Node(document_node_id, rank=rank)
                # document_node = pydot.Node(document_node_id, shape="box")
                self.add_node(document_node, rank_before)

                persons = self.graph[document]
                for person in persons:
                    # person_node_id = self.generate_person_node_id(person, document, time)
                    person_node_id = self.generate_person_unique_node_id(person, time)

                    # Already added in other function
                    person_node = pydot.Node(person_node_id, rank=self.time_to_ranks[time][1])
                    self.add_node(person_node, rank_after)

                    edge = pydot.Edge(document_node_id, person_node_id, **{EDGETYPE_KEY: "document_mention"})
                    self.pydot_graph.add_edge(edge)

            self.pydot_graph.add_subgraph(rank_after)
            self.pydot_graph.add_subgraph(rank_before)

    def dump_dot(self, dir_path="../layouts/"):
        nojump = "" if self.PERSON_JUMPS else "_nojump"
        align_persons = "_personAlign" if self.ALIGN_PERSONS else ""

        self.pydot_graph.write(f"{dir_path}test_bipartite{nojump}{align_persons}.dot")
        self.pydot_graph.write(f"{dir_path}test_bipartite{nojump}{align_persons}.svg", format="svg")

    def dump_txt(self):
        self.txt_path = f"dump.txt"
        self.pydot_graph.write(self.txt_path, format="plain")
        # svg = self.pydot_graph.create_svg()

    def node_id_before(self, name):
        return f"{name}_before"

    def node_id_after(self, name):
        return f"{name}_after"


if __name__ == "__main__":
    import json
    import DotLayoutParser
    from BipDynGraph import BipartiteDynGraph

    fp = "../data/Rolla_2modes.json"
    with open(fp) as file:
        json_data = json.load(file)

    graph = BipartiteDynGraph(json_data)

    layout_bipartite = PydotBipartiteLayout(graph)
    layout_bipartite.run()
    layout_bipartite.dump_dot()

    dot_parser = DotLayoutParser.DotLayoutParser(layout_bipartite, graph)
    # dot_parser.run_from_txt()
    # dot_parser.run_from_svg()
    dot_parser.run()

    json_graph = dot_parser.to_json()
    print(json_graph)

    path = "layeredGraph.json"
    with open(path, "w+") as f:
        json.dump(json_graph, f)
